#include <stdbool.h>
#include <stdio.h>

#include "util.h"
#include "bmp-converter.h"
#include "file-manager.h"

void check_error_open(enum open_file_status status, const char* file_name) {
    if (status) {
        err("%s. File %s.\n", open_file_msg[status], file_name);
    }
}

void check_error_read(enum read_status status, const char* file_name) {
    if (status) {
        err("%s. File %s.\n", read_status_msg[status], file_name);
    }
}

struct image read_image_from_file(const char* file_name) {
    struct image img = { 0 };
    FILE* file_in = NULL;
    enum open_file_status open_file_read = open_file_to_read_bytes(file_name, &file_in);
    check_error_open(open_file_read, file_name);
    enum read_status read_status = from_bmp(&img, file_in);
    check_error_read(read_status, file_name);
    closeFile(file_in);
    return img;
}

void save_image_to_file(struct image img, const char* file_name) {
    FILE* file_out = NULL;
    enum open_file_status open_file_write = open_file_to_write_bytes(file_name, &file_out);
    check_error_open(open_file_write, file_name);

    enum write_status write_status = to_bmp(file_out, img);
    if (write_status) {
        err("Could not write updated file.\n");
    }
    closeFile(file_out);
}

void check_args(int argc) {
    if (argc < 2) {
        err("Not enough arguments \n" );
    }
    if (argc > 4) {
        err("Too many arguments \n" );
    }
}

int main( int argc, char** argv ) {
    check_args(argc);

    char* filename_in;
    filename_in = argv[1];
    struct image image_old = read_image_from_file(filename_in);

    char* filename_out;
    if (argc == 2) {
        filename_out = filename_in;
    }
    if (argc == 3) {
        filename_out = argv[2];
    }

    struct image image_rotated = image_rotate_90(image_old, true);
    save_image_to_file(image_rotated, filename_out);
    return 0;
}
