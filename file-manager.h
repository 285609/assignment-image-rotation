#include <stdio.h>
#include <stdbool.h>

#ifndef IMAGE_ROTATION_FILE_MANAGER_H
#define IMAGE_ROTATION_FILE_MANAGER_H

enum open_file_status {
    OPEN_OK = 0,
    OPEN_NULL_FILENAME,
    OPEN_ERROR,
    OPEN_NO_SUCH_FILE,
    OPEN_PERMISSION_DENIED
};

static char* const open_file_msg[] = {
//    [READ_OK] = NULL,
        [OPEN_NULL_FILENAME] = "File name not specified",
        [OPEN_NO_SUCH_FILE] = "Could not find the file",
        [OPEN_PERMISSION_DENIED] = "File access denied",
        [OPEN_ERROR] = "Could not open file"
};

enum open_file_status open_file_to_read_bytes(const char* filename, FILE** f);

bool closeFile(FILE* f);

enum open_file_status open_file_to_write_bytes(const char* filename, FILE** f);


#endif //IMAGE_ROTATION_FILE_MANAGER_H
