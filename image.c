#include <stdio.h>
#include <stdlib.h>
#include "image.h"
#include "util.h"
#include "bmp.h"

#define PI 3.14159265358979323846

void pixel_print(struct pixel pixel) {
    char* b; char* g; char* r;
    b = dec_to_hex_str(pixel.b);
    g = dec_to_hex_str(pixel.g);
    r = dec_to_hex_str(pixel.r);
    printf("[%s %s %s]", b, g, r);
    free(b); free(g); free(r);
}

void image_print_pixels(struct image image) {
    for (size_t i = 0; i < image.width * image.height; i++) {
        pixel_print(image.data[i]);
        if ((i+1) % image.width == 0) printf("\n");
        else printf(" ");
    }
}

const struct pixel pixel_white = { 0xFF, 0xFF, 0xFF };

struct image* image_create(uint64_t width, uint64_t height) {
    size_t count = width * height;
    struct image* img = &(struct image){
            .width = width,
            .height = height,
            .data = malloc(sizeof(struct pixel) * count)};
    return img;
}

void image_init(struct image* img, uint64_t width, uint64_t height) {
    size_t count = width * height;
    *img = (struct image) {
        .width = width,
        .height = height,
        .data = malloc(sizeof(struct pixel) * count)
    };
    for (size_t i = 0; i < count; i++) {
        img->data[i] = pixel_white;
    }
}

void image_destroy(struct image img) {
    free(img.data);
}

size_t get_byte_skip(size_t img_width, size_t color_depth) {
    size_t byteSkip;
    if (img_width * color_depth % 4 == 0) {
        byteSkip = 0;
    } else {
        byteSkip = 4 - (img_width * color_depth % 4);
    }
    return byteSkip;
}

struct bmp_header image_generate_bmp_header(const struct image img) {
    const size_t count_px = img.width * img.height;
    struct bmp_header header = { 0 };
    header.bfType = 0x4D42;
    header.biSizeImage = sizeof(struct pixel) * count_px + img.height * get_byte_skip(img.width, 3);
    header.bfileSize = sizeof(struct bmp_header) + header.biSizeImage;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = img.width;
    header.biHeight = img.height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    return header;
}

struct int_coords transform_anticlockwise_coords(struct int_coords coords_old,
        int64_t img_width, double alpha) {
    struct int_coords coords_new = { 0 };
    const int64_t x_old = coords_old.x;
    const int64_t y_old = coords_old.y;
    const double sin_a = sin((double) alpha);
    const double cos_a = cos((double) alpha);
    const double width_dec = (double) (img_width - 1);
    coords_new.x = round((x_old - width_dec * sin_a * sin_a) * cos_a
            - (y_old + width_dec * sin_a * cos_a) * sin_a);
    coords_new.y = round((x_old - width_dec * sin_a * sin_a) * sin_a
                    + (y_old + width_dec * sin_a * cos_a) * cos_a);
    return coords_new;
}

struct int_coords transform_clockwise_coords(struct int_coords coords_old,
                                                 int64_t img_height, double alpha) {
    struct int_coords coords_new = { 0 };
    const int64_t x_old = coords_old.x;
    const int64_t y_old = coords_old.y;
    const double sin_a = sin((double) alpha);
    const double cos_a = cos((double) alpha);
    const double height_dec = (double) (img_height - 1);
    coords_new.x = round((x_old + height_dec * sin_a * cos_a) * cos_a
                   - (y_old - height_dec * sin_a * sin_a) * sin_a);
    coords_new.y = round((x_old + height_dec * sin_a * cos_a) * sin_a
                   + (y_old - height_dec * sin_a * sin_a) * cos_a);
    return coords_new;
}

double deg_to_rad(double alpha_deg) {
    return alpha_deg * PI / 180;
}

double round_to_six_n(double number) {
    const int64_t accuracy = 1000000;
    int64_t n = ceil(number * accuracy);
    return (double) n / accuracy;
}

struct image image_rotate_alpha_deg(const struct image oldImg, double alpha_deg) {
    const bool clockwise = alpha_deg <= 0;
    const double alpha = -deg_to_rad(alpha_deg);

    const double sin_a = round_to_six_n(sin((double) alpha));
    const double cos_a = round_to_six_n(cos((double) alpha));

    size_t newWidth = round(fabs(oldImg.width * cos_a) + fabs(oldImg.height * sin_a));
    size_t newHeight = round(fabs(oldImg.width * sin_a) + fabs(oldImg.height * cos_a));
    struct image newImg = {0};
    image_init(&newImg, newWidth, newHeight);

    struct int_coords coords_old = { 0 };
    for (coords_old.y = 0; coords_old.y < oldImg.height; coords_old.y += 1) {
        for (coords_old.x = 0; coords_old.x < oldImg.width; coords_old.x += 1) {
            const struct pixel pixel = pixel_get_by_coords(oldImg.data, oldImg.width, coords_old);
            struct int_coords coords_new;
            if (clockwise) {
                coords_new = transform_clockwise_coords(coords_old, oldImg.height, alpha);
            } else {
                coords_new = transform_anticlockwise_coords(coords_old, oldImg.width, alpha);
            }
//            printf("(%ld, %ld) -> (%ld, %ld)\t", coords_old.x, coords_old.y, coords_new.x, coords_new.y);
            pixel_set_by_coords(newImg.data, newWidth, coords_new, pixel);
        }
    }
//    image_print_pixels(newImg);
    return newImg;
}

struct image image_rotate_90(const struct image oldImg, bool clockwise) {
    const double alpha_deg = clockwise ? -90 : 90;
    return image_rotate_alpha_deg(oldImg, alpha_deg);
}

struct image image_rotate_180(const struct image oldImg) {
    return image_rotate_90(image_rotate_90(oldImg, true), true);
}


