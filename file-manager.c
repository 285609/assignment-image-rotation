#include <stdlib.h>
#include <errno.h>
#include "util.h"
#include "file-manager.h"

enum open_file_status return_error(int code) {
    switch (code) {
        case 2:
            return OPEN_NO_SUCH_FILE;
        case 13:
            return OPEN_PERMISSION_DENIED;
        default:
            perror("open_file");
            printf("%d", code); endl();
            return OPEN_ERROR;
    }
}

enum open_file_status open_file_to_read_bytes(const char* filename, FILE** f) {
    errno = 0;
    if (!filename) {
        return OPEN_NULL_FILENAME;
    }
    *f = fopen( filename, "rb" );
    if (!*f) {
        return return_error(errno);
    }
    return OPEN_OK;
}

enum open_file_status open_file_to_write_bytes(const char* filename, FILE** f) {
    errno = 0;
    if (!filename) return OPEN_NULL_FILENAME;
    *f = fopen( filename, "wb" );
    if (!*f) {
        return return_error(errno);

    }
    return OPEN_OK;
}

bool closeFile(FILE* f) {
    return fclose(f);
}
