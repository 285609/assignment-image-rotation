cmake_minimum_required(VERSION 3.16.3)
project(Image_Rotation C)
set(CMAKE_C_STANDARD 11)


add_executable(Image_Rotation main.c image.c image.h util.c util.h bmp-converter.c
        bmp-converter.h file-manager.c file-manager.h bmp.h)
target_link_libraries(Image_Rotation m)
