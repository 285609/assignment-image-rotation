#include <inttypes.h>
#include <stdio.h>

#ifndef IMAGE_ROTATION_UTIL_H
#define IMAGE_ROTATION_UTIL_H

_Noreturn void err( const char* msg, ... );

struct int_coords {
    int64_t x, y;
};

void print_size(size_t s);

void print_int64(int64_t i);

void endl();

struct pixel* get_n_row(struct pixel* pixels, size_t width, size_t n);

struct pixel pixel_get_by_coords(struct pixel* pixels, size_t pixels_in_row, struct int_coords coord);

void pixel_set_by_coords(struct pixel* pixels, size_t pixels_in_row,
        struct int_coords coord, struct pixel pixel);

char* dec_to_hex_str(uint64_t i);

#endif //IMAGE_ROTATION_UTIL_H
