#include <math.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"

#include "image.h"
_Noreturn void err( const char* msg, ... ) {
    va_list args;
    va_start (args, msg);
    vfprintf(stderr, msg, args);
    va_end (args);
    exit(1);
}

void print_size(size_t s) {
    printf("%zu", s);
}

void print_int64(int64_t i) {
    printf("%" PRId64, i);
}

void endl() {
    printf("\n");
}

struct pixel* get_n_row(struct pixel* const pixels, size_t width, size_t n) {
    return (pixels + (width * n));
}

struct pixel pixel_get_by_coords(struct pixel* const pixels, size_t pixels_in_row,
                                  const struct int_coords coord) {
    struct pixel* row_y = get_n_row(pixels, pixels_in_row, coord.y);
    return row_y[coord.x];
}

void pixel_set_by_coords(struct pixel* const pixels, size_t pixels_in_row,
                         const struct int_coords coord, const struct pixel pixel) {
    struct pixel* row_y = get_n_row(pixels, pixels_in_row, coord.y);
    row_y[coord.x] = pixel;
}

char* dec_to_hex_str(uint64_t i) {
    char* hex = "0123456789abcdef";
    size_t digits = i == 0 ? 1 : ceil(0.25 * log2(i));
    int64_t r;
    char result[digits];
    if (digits == 0) {
        result[0] = '0';
    } else {
        for (size_t j = 0; j < digits; j++) {
            r = i % 16;
            i /= 16;
            result[j] = hex[r];
        }
    }

    char* result_r = malloc(sizeof(char) * digits + 1);
    for (size_t j = 0; j < digits; j++) {
        result_r[j] = result[digits-1-j];
    }
    result_r[digits] = '\0';
    return result_r;

}