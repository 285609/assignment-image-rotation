#ifndef IMAGE_ROTATION_IMAGE_H
#define IMAGE_ROTATION_IMAGE_H

#include <stdint.h>
#include <malloc.h>
#include <math.h>
#include <stdbool.h>

struct pixel {
    uint8_t b, g, r;
};

extern const struct pixel pixel_white;

void pixel_print(struct pixel pixel);

struct image {
    int64_t width, height;
    struct pixel* data;
};

void image_print_pixels(struct image image);

struct image* image_create(uint64_t width, uint64_t height);

void image_init(struct image* img, uint64_t width, uint64_t height);

void image_destroy(struct image img);

struct bmp_header image_generate_bmp_header(struct image img);

//struct image image_rotate_alpha_deg(struct image oldImg, double_t alpha);

struct image image_rotate_90(struct image oldImg, bool clockwise);

struct image image_rotate_180(struct image oldImg);


#endif //IMAGE_ROTATION_IMAGE_H
