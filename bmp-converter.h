#ifndef IMAGE_ROTATION_BMP_CONVERTER_H
#define IMAGE_ROTATION_BMP_CONVERTER_H

#include <stdio.h>
#include "image.h"

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BIT_COUNT,
    READ_INVALID_HEADER,
    READ_ERROR
};

static char* const read_status_msg[] = {
//    [READ_OK] = NULL,
    [READ_INVALID_SIGNATURE] = "Unknown file format",
    [READ_INVALID_BIT_COUNT] = "Invalid color depth",
    [READ_INVALID_HEADER] = "File is corrupted",
    [READ_ERROR] = "Read error"
};

enum read_status from_bmp( struct image* img, FILE* file_in );

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp( FILE* out, struct image img );


#endif //IMAGE_ROTATION_BMP_CONVERTER_H
