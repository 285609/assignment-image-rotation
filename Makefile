CFLAGS=--std=c11 -Wall -pedantic -Isrc/ -ggdb -Wextra -DDEBUG
CC=gcc
LIBS += -lm

all: image_rotator

util.o: util.c
	$(CC) -c $(CFLAGS) $< -o $@

image.o: image.c
	$(CC) -c $(CFLAGS) $< -o $@ $(LIBS)

bmp-converter.o: bmp-converter.c
	$(CC) -c $(CFLAGS) $< -o $@

file-manager.o: file-manager.c
	$(CC) -c $(CFLAGS) $< -o $@

main.o: main.c
	$(CC) -c $(CFLAGS) $< -o $@


image_rotator: main.o util.o file-manager.o bmp-converter.o image.o
	$(CC) -o image_rotator $^ $(LIBS)

clean:
	rm -f main.o util.o file-manager.o bmp-converter.o image.o image_rotator

