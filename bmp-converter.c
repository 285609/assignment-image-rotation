#include <inttypes.h>
#include <stdio.h>
#include "bmp-converter.h"
#include "bmp.h"
#include "util.h"

bool read_header( FILE* f, struct bmp_header* header ) {
    return fread( header, sizeof( struct bmp_header ), 1, f );
}

bool read_header_from_opened_file( FILE* in, struct bmp_header* header ) {
    if (!in) return false;
    if (read_header( in, header ) ) {
        return true;
    }
    return false;
}

enum read_status check_header(const struct bmp_header header) {
    if (header.bfType != 0x4D42 && header.bfType != 0x424D) {
        return READ_INVALID_SIGNATURE;
    }
    if (header.bfReserved != 0 || header.bOffBits < sizeof(header) || header.biPlanes != 1) {
        return READ_INVALID_HEADER;
    }
    if (header.biBitCount != 24) {
        return READ_INVALID_BIT_COUNT;
    }
    return READ_OK;
}

bool read_array(FILE* in, void* arr, size_t size_of, size_t n) {
    return fread(arr, size_of, n, in);
}

void skip_n_byte(FILE* in, size_t n) {
    uint8_t* bytes = malloc(sizeof(uint8_t) * n);
    fread(bytes, sizeof(uint8_t), n, in);
    free(bytes);
}

void set_image_size_from_header(struct image* img, const struct bmp_header h) {
    const size_t widthPixel = h.biWidth;
    const size_t heightPixel = h.biHeight;
    image_init(img, widthPixel, heightPixel);
}

size_t get_byte_offset_without_header(const struct bmp_header header) {
    return header.bOffBits - sizeof(header);
}

size_t get_byte_skip_from_header(const struct bmp_header header, const struct image img) {
    size_t depthByte = header.biBitCount / 8;
    size_t byteSkip;
    if (img.width * depthByte % 4 == 0) {
        byteSkip = 0;
    } else {
        byteSkip = 4 - (img.width * depthByte % 4);
    }
    return byteSkip;
}

void pixel_set_row_by_y(struct pixel* const arr, size_t pixels_in_row,
                        int64_t y, const struct pixel* pixels) {
    struct pixel* arr_y_row = get_n_row(arr, pixels_in_row, y);
    for (size_t i = 0; i < pixels_in_row; i++) {
        arr_y_row[i] = pixels[i];
    }
}

void read_pixels_from_file(FILE* file_in, struct image* img, struct bmp_header h) {
    const size_t width = img->width;
    const size_t height = img->height;
    const size_t byte_skip = get_byte_skip_from_header(h, *img);
    
    const size_t byte_offset = get_byte_offset_without_header(h);
    if (byte_offset) skip_n_byte(file_in, byte_offset);
    
    struct pixel* read_pixels_row = malloc(sizeof(struct pixel) * width);
    for (int64_t y = height - 1; y >= 0; y -= 1) {
        read_array(file_in, read_pixels_row, sizeof(struct pixel), width);
        pixel_set_row_by_y(img->data, width, y, read_pixels_row);
        if (byte_skip) skip_n_byte(file_in, byte_skip);
    }
    free(read_pixels_row);
}

void write_n_rub_byte(FILE* out, size_t n) {
    uint8_t* bytes = malloc(sizeof(uint8_t) * n);
    fwrite(bytes, sizeof(uint8_t), n, out);
    free(bytes);
}

void write_header(FILE* out, struct bmp_header* header) {
    fwrite(header, sizeof(struct bmp_header), 1, out);
}

void write_image_to_file(FILE* out, const struct bmp_header header, const struct image img) {
    size_t byte_skip = get_byte_skip_from_header(header, img);
    for (int64_t y = img.height - 1; y >= 0; y--) {
        struct pixel *cur_row = get_n_row(img.data, img.width, y);
        fwrite(cur_row, sizeof(struct pixel), img.width, out);
        if (byte_skip) write_n_rub_byte(out, byte_skip);
    }
}

enum read_status from_bmp(struct image* const img, FILE* file_in ) {
    struct bmp_header h = { 0 };
    if (read_header_from_opened_file(file_in, &h )) {
        enum read_status check_status = check_header(h);
        if (check_status) return check_status;
        else {
            set_image_size_from_header(img, h);
            read_pixels_from_file(file_in, img, h);
            return READ_OK;
        }
    } else
        return READ_ERROR;
}

enum write_status to_bmp( FILE* out, const struct image img ) {
    if (!out) return WRITE_ERROR;
    struct bmp_header h = image_generate_bmp_header(img);
    write_header(out, &h);
    write_image_to_file(out, h, img);
    return WRITE_OK;
}



